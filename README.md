# Introduction
Wrapper module for the community module `mrfioc2`.

# EVR IOC parameters
```
## PEVR - prefix EVR
## PCIID - pci address
## NTP_ENA - enable EVR as NTP source
```
