require "mrfioc2" "$(SwVer=)"
require "essioc"

# Config
epicsEnvSet "PEVR" "$(PEVR=TDT:Ctrl-EVR-1:)"
epicsEnvSet "PCIID" "$(PCIID=0e:00.0)"
epicsEnvSet "IOCNAME" "$(IOCNAME=TDT:SC-IOC-1)"

# Common config
epicsEnvSet "DEBUG" ""

# Select one EVR type
## EVR MTCA
iocshLoad "$(mrfioc2ess_DIR)/evrEss.iocsh" "P=$(PEVR)"
## EVR PCIe
# iocshLoad "$(mrfioc2ess_DIR)/evrEss.iocsh" "P=$(PEVR),EVRDB=evr-pcie-300dc.univ.db"
## EVR PCIe - default
# iocshLoad "$(mrfioc2ess_DIR)/evrEss.iocsh" "P=$(PEVR),EVRDB=evr-pcie-300dcs.univ.db"

# Common
iocshLoad "$(essioc_DIR)/common_config.iocsh"
