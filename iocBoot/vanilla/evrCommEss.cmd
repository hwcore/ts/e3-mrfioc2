#!/epics-vanilla/testing/siteMods/mrfioc2/bin/linux-x86_64/mrf
epicsEnvSet "EPICS_BASE" "/epics-vanilla/testing"
epicsEnvSet "mrfioc2ess_DIR" "$(EPICS_BASE)/siteMods/e3-mrfioc2"

epicsEnvSet "PEVR"  "$(PEVR=VANESS:EVR-1:)"
epicsEnvSet "PCIID" "$(PCIID=09:00.0)"

iocshLoad "$(mrfioc2ess_DIR)/evrEss.iocsh" "P=$(PEVR),VAN="

iocInit
