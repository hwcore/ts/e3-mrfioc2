#!/epics-vanilla/testing/siteMods/mrfioc2/bin/linux-x86_64/mrf
epicsEnvSet "EPICS_BASE" "/epics-vanilla/testing"

epicsEnvSet "TOP" "$(EPICS_BASE)/siteMods/mrfioc2"
dbLoadDatabase $(TOP)/dbd/mrf.dbd
mrf_registerRecordDeviceDriver(pdbbase)

# Config
# MTCA-9U
#epicsEnvSet "PCIID" "08:00.0"
#epicsEnvSet "PEVR" "TDL-JJ:EVR-1:"
#epicsEnvSet "PCIID" "0a:00.0"
#epicsEnvSet "PEVR" "TDL-JJ:EVR-2"
#epicsEnvSet "PCIID" "09:00.0"

epicsEnvSet "PEVR" "VAN:EVR-1:"
epicsEnvSet "PCIID" "$(PCIID=09:00.0)"

var evrMrmTimeNSOverflowThreshold 60000000

mrmEvrSetupPCI "EVR" "$(PCIID)"
#dbLoadRecords "db/evr-base.univ.db"                  "P=$(PEVR),EVR=$(EVR=EVR),FEVT=$(FEVT=88.0525)"
#dbLoadRecords "db/$(EVRDB=evr-pcie-300dcs.univ.db)"  "P=$(PEVR),EVR=$(EVR=EVR),FEVT=$(FEVT=88.0525)"
dbLoadRecords "$(TOP)/db/evr-mtca-300u.uv.db"         "P=$(PEVR),EVR=$(EVR=EVR),FEVT=$(FEVT=88.0525)"

iocInit

dbpf "$(PEVR)DCTgt-SP" "$(DlyComp=10000)"
dbpf "$(PEVR)DCEna-Sel" "Enable"
dbpf "$(PEVR)TimeSrc-Sel" "0"
