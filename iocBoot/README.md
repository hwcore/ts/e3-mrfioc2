# Usage example
```
$ cd [ioc_root] # Any directory e.g. /var/tmp/evr-1
$ PEVR=[IOCPREFIX] PCIID=[AdressOfEvr] [path_to_mrfioc2]/evrEss.cmd # For a particular EVR e.g. PEVR=TDL-JJ:Ctrl-EVR-2 PCIID=09:00.0 [path_to_mrfioc2]/evrEss.cmd
```
## Practical example
```
cd /var/tmp/evr-1
source /epicsts/base-7.0.6.1/require/4.0.0/bin/setE3Env.bash
PEVR=MEBT-010:RFS-EVR-101 PCIID=0b:00.0 /epicsts/base-7.0.6.1/require/4.0.0/siteMods/mrfioc2/2.3.1+51/evrEss.cmd
```
# With cellmod
```
$ cd [ioc_root]
$ PEVR=[IOCPREFIX] PCIID=[AdressOfEvr] iocsh -l /var/tmp/cellMods [path_to_mrfioc2]/evrEss.cmd
```
# Parameters
```
# Common config
epicsEnvSet "DEBUG" "" # If local debugging required
# Select EVR
## No params == test evr
## epicsEnvSet TDL ""  # Timing Distribution Labs
## epicsEnvSet TD ""   # Timing Distribution
```
