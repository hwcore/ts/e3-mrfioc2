
define target_meta
	@echo "============================================================================="
	@echo $(lastword $(MAKEFILE_LIST))	"Building target: $@"
	@echo "============================================================================="
endef

EVG_SUBS := $(wildcard $(EVGMRMAPPDB)/*.substitutions)
EVG_TMPS := $(wildcard $(EVGMRMAPPDB)/*.template)
EVR_SUBS := $(wildcard $(EVRMRMAPPDB)/*.substitutions)

ESS_SUBS := $(wildcard $(E3MOD_ROOT)/template/*.substitutions)
ESS_SUBS += $(wildcard $(E3MOD_ROOT)/template/*/*.substitutions)

#SUBS += $(EVG_SUBS) $(EVR_SUBS) $(ESS_SUBS)
#TMPS += $(EVG_TMPS)

.PHONY: $(EVG_SUBS) $(EVR_SUBS) $(ESS_SUBS)
$(EVG_SUBS) $(EVR_SUBS) $(ESS_SUBS):
	@printf "SUBS inflating: %48s >>> %40s \n" "$@" "$(basename $(@)).db"
	@rm -f  $(basename $(@)).db.d  $(basename $(@)).db
	@$(MSI) -D $(USR_DBFLAGS) -o $(basename $(@)).db -S $@  > $(basename $(@)).db.d
	@$(MSI)    $(USR_DBFLAGS) -o $(basename $(@)).db -S $@

.PHONY: $(EVG_TMPS)
$(EVG_TMPS):
	@printf "TMPS inflating: %48s >>> %40s \n" "$@" "$(basename $(@)).db"
	@rm -f  $(basename $(@)).db.d  $(basename $(@)).db
	@$(MSI) -D $(USR_DBFLAGS) -o $(basename $(@)).db $@  > $(basename $(@)).db.d
	@$(MSI)    $(USR_DBFLAGS) -o $(basename $(@)).db $@

INFO_META_FILE:=$(E3MOD_ROOT)/build/info.meta
#$(eval E3_VERSION := $$(cd $(E3MOD_ROOT) && git rev-parse HEAD))

SCRIPTS += $(INFO_META_FILE)

.PHONY: info_meta
info_meta:
	@mkdir -p $(E3MOD_ROOT)/build
	@echo "<META_HEADER>" > $(INFO_META_FILE)
	@echo GCCVERSION $$(gcc -dumpversion) BUILD_DATE $$(date +'%Y-%m-%dT%H:%M:%S%z') >> $(INFO_META_FILE)
	@echo EPICS_BASE $(EPICS_BASE) E3_REQUIRE_NAME $(E3_REQUIRE_NAME) E3_REQUIRE_VERSION  $(E3_REQUIRE_VERSION) >> $(INFO_META_FILE)
	@echo DEVLIB2_DEP_VERSION $(DEVLIB2_DEP_VERSION) >> $(INFO_META_FILE)
	@echo EPICS_MODULE_TAG $(EPICS_MODULE_TAG) E3_MODULE_VERSION $(E3_MODULE_VERSION) >> $(INFO_META_FILE)
	@echo "$(E3_MODULE_NAME) URL $$(git remote get-url origin | sed 's/\/\/.*@/\/\//')" >> $(INFO_META_FILE)
	@echo "$(E3_MODULE_NAME) BRANCH_NAME $$(git symbolic-ref --short HEAD)" >> $(INFO_META_FILE)
	@echo "$(E3_MODULE_NAME) COMMIT_HASH $$(git rev-parse HEAD)" >> $(INFO_META_FILE)
	@echo "e3-$(E3_MODULE_NAME) URL $$(cd $(E3MOD_ROOT) && git remote get-url origin)" >> $(INFO_META_FILE)
	@echo "e3-$(E3_MODULE_NAME) BRANCH_NAME $$(cd $(E3MOD_ROOT) && git symbolic-ref --short HEAD)" >> $(INFO_META_FILE)
	@echo "e3-$(E3_MODULE_NAME) COMMIT_HASH $$(cd $(E3MOD_ROOT) && git rev-parse HEAD)" >> $(INFO_META_FILE)
	@echo GCCSYSINFO >> $(INFO_META_FILE)
	@gcc -E -v - </dev/null >> $(INFO_META_FILE) 2>&1
	@cat $(INFO_META_FILE)

.PHONY: dbclean
dbclean:
	#rm $(E3MOD_ROOT)/mrfioc2/*_Common/* || true
	rm $(E3MOD_ROOT)/template/*.db* || true
	rm $(E3MOD_ROOT)/template/*/*.db* || true

# Apply all prebuild targets
.PHONY: prebuild
prebuild: \
	dbclean \
	$(EVG_SUBS) $(EVR_SUBS) $(ESS_SUBS) $(EVG_TMPS) \
	info_meta
