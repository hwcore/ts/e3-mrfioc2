# .cmd
# ====

require "mrfioc2" "2.2.1rc1"

epicsEnvSet "EPICS_CA_MAX_ARRAY_BYTES" "10000000"

epicsEnvSet "TOP" "$(E3_IOCSH_TOP)/.."

iocshLoad "$(TOP)/iocsh/img.iocsh" "DEV=$(DEV), EVM=$(EVM=#), EVR=$(EVR=#), BIN_WR=$(BIN_WR=#), PCIID=$(PCI_SLOT), IMG_FILE=$(IMG_FILE)"

exit

# EOF
