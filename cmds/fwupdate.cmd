
require "mrfioc2" "2.3.1-beta.5"
epicsEnvSet("DEV1", "EVR1")
mrmEvrSetupPCI("$(DEV1)", "0e:00.0")
iocInit()
# Remove security check for downgrade firmware
var("flashAcknowledgeMismatch", 1)
# EVR file location
flashwrite("EVR1:FLASH", 0, "/home/iocuser/evr.bit")
