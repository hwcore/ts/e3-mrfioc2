#
#  Copyright (c) 2017 - Present  Jeong Han Lee
#  Copyright (c) 2017 - Present  European Spallation Source ERIC
#
#  The program is free software: you can redistribute
#  it and/or modify it under the terms of the GNU General Public License
#  as published by the Free Software Foundation, either version 2 of the
#  License, or any newer version.
#
#  This program is distributed in the hope that it will be useful, but WITHOUT
#  ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
#  FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for
#  more details.
#
#  You should have received a copy of the GNU General Public License along with
#  this program. If not, see https://www.gnu.org/licenses/gpl-2.0.txt
#
# Author  : Jeong Han Lee
# email   : han.lee@esss.se
# Date    : Tuesday, March 12 13:41:50 CET 2019
# version : 0.0.7
#

where_am_I := $(dir $(abspath $(lastword $(MAKEFILE_LIST))))
include ${E3_REQUIRE_TOOLS}/driver.makefile

ifneq ($(strip $(DEVLIB2_DEP_VERSION)),)
	devlib2_VERSION:=$(DEVLIB2_DEP_VERSION)
endif

# Exclude architerctures that are not in use
EXCLUDE_ARCHS += linux-ppc64e6500

# Duplicated one with 3.0.1
# mrfioc2/configure/CONFIG_SITE
USR_CPPFLAGS += -DUSE_TYPED_RSET

# mrfCommon/Makefile
USR_CFLAGS   += -DDEBUG_PRINT
USR_CPPFLAGS += -DDEBUG_PRINT

USR_CXXFLAGS += -Wno-cpp

evgMrm_CPPFLAGS += -DMRF_VERSION=\"$(E3_MODULE_VERSION)\"
evgMrm_CPPFLAGS += -D__COMMIT_HASH=\"$$(git rev-parse --short=8 HEAD)\"

evr_CPPFLAGS += -DMRF_VERSION=\"$(E3_MODULE_VERSION)\"
evr_CPPFLAGS += -D__COMMIT_HASH=\"$$(git rev-parse --short=8 HEAD)\"

# Add EVRU/D irqpolling to EVM
evgInit_CPPFLAGS += -D__IRQP_EVRU_PRD=0.01
#evgInit_CPPFLAGS += -D__IRQP_EVRD_PRD=0.01

# COMMUNITY Dependency
# mrfCommon (mrfioc2), mrmShared (mrfioc2), epicsvme (devlib2), epicspci (devlib2)
EVGMRMAPP:= evgMrmApp
EVGMRMAPPSRC:=$(EVGMRMAPP)/src
EVGMRMAPPDB:=$(EVGMRMAPP)/Db

SOURCES += $(EVGMRMAPPSRC)/evgInit.cpp

SOURCES += $(EVGMRMAPPSRC)/evg.cpp

SOURCES += $(EVGMRMAPPSRC)/evgMrm.cpp

SOURCES += $(EVGMRMAPPSRC)/evgAcTrig.cpp

SOURCES += $(EVGMRMAPPSRC)/evgEvtClk.cpp

SOURCES += $(EVGMRMAPPSRC)/evgTrigEvt.cpp
SOURCES += $(EVGMRMAPPSRC)/devSupport/devEvgTrigEvt.cpp

SOURCES += $(EVGMRMAPPSRC)/evgMxc.cpp

SOURCES += $(EVGMRMAPPSRC)/evgDbus.cpp
SOURCES += $(EVGMRMAPPSRC)/devSupport/devEvgDbus.cpp

SOURCES += $(EVGMRMAPPSRC)/evgInput.cpp

SOURCES += $(EVGMRMAPPSRC)/evgOutput.cpp

SOURCES += $(EVGMRMAPPSRC)/fct.cpp
SOURCES += $(EVGMRMAPPSRC)/mrmevgseq.cpp

SOURCES += $(EVGMRMAPPSRC)/seqconst.c
SOURCES += $(EVGMRMAPPSRC)/seqnsls2.c

DBDS    += $(EVGMRMAPPSRC)/evgInit.dbd

# HEADERS += $(EVGMRMAPPSRC)/evgMrm.h
# HEADERS += $(EVGMRMAPPSRC)/evgRegMap.h
# HEADERS += $(EVGMRMAPPSRC)/evgAcTrig.h
# HEADERS += $(EVGMRMAPPSRC)/evgEvtClk.h
# HEADERS += $(EVGMRMAPPSRC)/evgTrigEvt.h
# HEADERS += $(EVGMRMAPPSRC)/evgMxc.h
# HEADERS += $(EVGMRMAPPSRC)/evgDbus.h
# HEADERS += $(EVGMRMAPPSRC)/evgInput.h
# HEADERS += $(EVGMRMAPPSRC)/evgOutput.h
# HEADERS += $(EVGMRMAPPSRC)/mrmevgseq.h
# HEADERS += $(EVGMRMAPPSRC)/fct.h


# COMMUNITY Dependency
# mrfCommon (mrfioc2), mrmShared (mrfioc2), evr (mrfioc2), epicsvme (devlib2), epicspci (devlib2)
EVRMRMAPP:= evrMrmApp
EVRMRMAPPSRC:=$(EVRMRMAPP)/src
EVRMRMAPPDB:=$(EVRMRMAPP)/Db


SOURCES += $(EVRMRMAPPSRC)/drvemIocsh.cpp
SOURCES += $(EVRMRMAPPSRC)/drvemSetup.cpp
SOURCES += $(EVRMRMAPPSRC)/drvem.cpp
SOURCES += $(EVRMRMAPPSRC)/drvemOutput.cpp
SOURCES += $(EVRMRMAPPSRC)/drvemInput.cpp
SOURCES += $(EVRMRMAPPSRC)/drvemPrescaler.cpp
SOURCES += $(EVRMRMAPPSRC)/drvemPulser.cpp
SOURCES += $(EVRMRMAPPSRC)/drvemCML.cpp
SOURCES += $(EVRMRMAPPSRC)/drvemTSBuffer.cpp
SOURCES += $(EVRMRMAPPSRC)/delayModule.cpp
SOURCES += $(EVRMRMAPPSRC)/drvemRxBuf.cpp
SOURCES += $(EVRMRMAPPSRC)/devMrmBuf.cpp

SOURCES += $(EVRMRMAPPSRC)/mrmevrseq.cpp

SOURCES += $(EVRMRMAPPSRC)/bufrxmgr.cpp
SOURCES += $(EVRMRMAPPSRC)/devMrmBufRx.cpp

SOURCES += $(EVRMRMAPPSRC)/os/default/irqHack.cpp

SOURCES += $(EVRMRMAPPSRC)/mrmGpio.cpp

DBDS    += $(EVRMRMAPPSRC)/drvemSupport.dbd





# # COMMUNITY Dependency

# mrfCommon
EVRAPP:= evrApp
EVRAPPSRC:=${EVRAPP}/src
EVRAPPDB:=${EVRAPP}/Db

# HEADERS += $(EVRAPPSRC)/evr/pulser.h
# HEADERS += $(EVRAPPSRC)/evr/output.h
# HEADERS += $(EVRAPPSRC)/evr/delay.h
# HEADERS += $(EVRAPPSRC)/evr/input.h
# HEADERS += $(EVRAPPSRC)/evr/prescaler.h
# HEADERS += $(EVRAPPSRC)/evr/evr.h
# HEADERS += $(EVRAPPSRC)/evr/cml.h



DBDS    += $(EVRAPPSRC)/evrSupport.dbd
DBDS 		+= $(EVRAPPSRC)/evrSupportBase7.dbd

SOURCES += $(EVRAPPSRC)/evr.cpp

# HEADERS += $(EVRAPPSRC)/evrGTIF.h
SOURCES += $(EVRAPPSRC)/evrGTIF.cpp

SOURCES += $(EVRAPPSRC)/devEvrStringIO.cpp

SOURCES += $(EVRAPPSRC)/devEvrEvent.cpp

SOURCES += $(EVRAPPSRC)/devEvrMapping.cpp

SOURCES += $(EVRAPPSRC)/devEvrPulserMapping.cpp

SOURCES += $(EVRAPPSRC)/asub.c
SOURCES += $(EVRAPPSRC)/devWfMailbox.c


SOURCES_Linux += $(EVRAPPSRC)/ntpShm.cpp
SOURCES_DEFAULT += $(EVRAPPSRC)/ntpShmNull.cpp



# COMMUNITY Dependency

# mrfCommon
MRMSHARED:= mrmShared
MRMSHAREDSRC:=${MRMSHARED}/src
MRMSHAREDDB:=${MRMSHARED}/Db


# HEADERS += $(MRMSHAREDSRC)/mrmDataBufTx.h
# HEADERS += $(MRMSHAREDSRC)/mrmSeq.h
# HEADERS += $(MRMSHAREDSRC)/mrmpci.h
# HEADERS += $(MRMSHAREDSRC)/sfp.h


SOURCES += $(MRMSHAREDSRC)/mrmDataBufTx.cpp
SOURCES += $(MRMSHAREDSRC)/mrmSeq.cpp
SOURCES += $(MRMSHAREDSRC)/devMrfBufTx.cpp
SOURCES += $(MRMSHAREDSRC)/sfp.cpp
SOURCES += $(MRMSHAREDSRC)/mrmtimesrc.cpp

# After 2.2.0, 2.2.0-ess-rc3,
# mrfioc2.Makefile needs to handel new file with the compatibility with
# old version up to 2.2.0-ess-rc2
SOURCES += $(MRMSHAREDSRC)/mrmspi.cpp

DBDS    += $(MRMSHAREDSRC)/mrmShared.dbd




MRFCOMMON:= mrfCommon/src

# HEADERS += $(MRFCOMMON)/mrfBitOps.h
# HEADERS += $(MRFCOMMON)/mrfCommon.h

# HEADERS += $(MRFCOMMON)/mrfCommonIO.h
# HEADERS += $(MRFCOMMON)/mrfFracSynth.h

# HEADERS += $(MRFCOMMON)/linkoptions.h
# HEADERS += $(MRFCOMMON)/mrfcsr.h

# HEADERS += $(MRFCOMMON)/mrf/databuf.h
# HEADERS += $(MRFCOMMON)/mrf/object.h

# HEADERS += $(MRFCOMMON)/mrf/version.h

# HEADERS += $(MRFCOMMON)/devObj.h

# HEADERS += $(MRFCOMMON)/configurationInfo.h
# HEADERS += $(MRFCOMMON)/plx9030.h
# HEADERS += $(MRFCOMMON)/plx9056.h
# HEADERS += $(MRFCOMMON)/latticeEC30.h

# HEADERS += $(MRFCOMMON)/mrf/version.h

DBDS    += $(MRFCOMMON)/mrfCommon.dbd

SOURCES += $(MRFCOMMON)/mrfFracSynth.c
SOURCES += $(MRFCOMMON)/linkoptions.c
SOURCES += $(MRFCOMMON)/object.cpp
SOURCES += $(MRFCOMMON)/devObj.cpp
SOURCES += $(MRFCOMMON)/devObjAnalog.cpp
SOURCES += $(MRFCOMMON)/devObjLong.cpp
SOURCES += $(MRFCOMMON)/devObjBinary.cpp
SOURCES += $(MRFCOMMON)/devObjMBB.cpp
SOURCES += $(MRFCOMMON)/devObjMBBDirect.cpp
SOURCES += $(MRFCOMMON)/devObjString.cpp
SOURCES += $(MRFCOMMON)/devObjCommand.cpp
SOURCES += $(MRFCOMMON)/devObjWf.cpp
SOURCES += $(MRFCOMMON)/devMbboDirectSoft.c
SOURCES += $(MRFCOMMON)/devlutstring.cpp
SOURCES += $(MRFCOMMON)/databuf.cpp
SOURCES += $(MRFCOMMON)/mrfCommon.cpp
SOURCES += $(MRFCOMMON)/spi.cpp
SOURCES += $(MRFCOMMON)/flash.cpp
SOURCES += $(MRFCOMMON)/flashiocsh.cpp
#NOTINUSE:
SOURCES += $(MRFCOMMON)/pollirq.cpp

# After 2.2.0, 2.2.0-ess-rc3,
# mrfioc2.Makefile needs to handel new file with the compatibility with
# old version up to 2.2.0-ess-rc2
SOURCES += $(MRFCOMMON)/pollirq.cpp

SOURCES += $(MRFCOMMON)/flashiocsh.cpp

# TODO: Absolute path makes too long targets
# E3MOD_ROOT:=$(where_am_I)/..
E3MOD_ROOT:=../

SCRIPTS += $(wildcard $(E3MOD_ROOT)/iocsh/*.*sh)
SCRIPTS += $(wildcard $(E3MOD_ROOT)/iocsh/*/*.*sh)
SCRIPTS += $(wildcard $(E3MOD_ROOT)/iocsh/*/*/*.*sh)
SCRIPTS += $(wildcard $(E3MOD_ROOT)/iocBoot/*.cmd)
SCRIPTS += $(wildcard $(E3MOD_ROOT)/iocBoot/*/*.cmd)
SCRIPTS += $(wildcard $(E3MOD_ROOT)/bin/*.bash)

TEMPLATES += $(wildcard $(MRMSHAREDDB)/*.db)
TEMPLATES += $(wildcard $(EVRAPPDB)/*.db)
TEMPLATES += $(wildcard $(EVGMRMAPPDB)/*.db)
TEMPLATES += $(wildcard $(EVRMRMAPPDB)/*.db)

TEMPLATES += $(wildcard $(MRMSHAREDDB)/*.template)
TEMPLATES += $(wildcard $(EVRAPPDB)/*.template)
TEMPLATES += $(wildcard $(EVGMRMAPPDB)/*.template)
TEMPLATES += $(wildcard $(EVRMRMAPPDB)/*.template)

#TODO: Check TEMPLATES += $(wildcard ../template/evr*.substitutions)
#TEMPLATES += $(wildcard $(E3MOD_ROOT)/template/*.template)
TEMPLATES += $(wildcard $(E3MOD_ROOT)/template/*.inc.template)
TEMPLATES += $(wildcard $(E3MOD_ROOT)/template/*/*.inc.template)
TEMPLATES += $(wildcard $(E3MOD_ROOT)/db/*.db)
TEMPLATES += $(wildcard $(E3MOD_ROOT)/db/*/*.db)
TEMPLATES += $(wildcard $(E3MOD_ROOT)/template/*.db)
TEMPLATES += $(wildcard $(E3MOD_ROOT)/template/*/*.db)

USR_DBFLAGS += -I . -I ..
USR_DBFLAGS += -I $(EPICS_BASE)/db
USR_DBFLAGS += -I $(MRMSHAREDDB)
USR_DBFLAGS += -I $(EVGMRMAPPDB)
USR_DBFLAGS += -I $(EVRMRMAPPDB)
USR_DBFLAGS += -I $(EVRAPPDB)

USR_DBFLAGS += -I $(E3MOD_ROOT)/db
USR_DBFLAGS += -I $(E3MOD_ROOT)/template
USR_DBFLAGS += $(addprefix -I, $(wildcard $(E3MOD_ROOT)/template/*/))

.PHONY: db
db:

#
.PHONY: vlibs
vlibs:
#

-include $(where_am_I)/../config.Makefile
-include $(where_am_I)/../config.Makefile.local
