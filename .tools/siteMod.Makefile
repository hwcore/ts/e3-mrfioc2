
TOP:=$(CURDIR)

include $(TOP)/configure/CONFIG_MODULE
include $(TOP)/configure/RELEASE

EPICS_BASE_VANILLA ?= /tmp/base-7.0.8/

BASE = $(word 2, $(subst /, ,$(EPICS_BASE)))
CellmodSeedInstallDir ?= $(EPICS_BASE_VANILLA)/siteMods/e3-$(EPICS_MODULE_NAME)
CellmodSeedDir ?= $(TOP)/cellMods/$(BASE)/$(E3_REQUIRE_NAME)-$(E3_REQUIRE_VERSION)/$(EPICS_MODULE_NAME)/$(E3_MODULE_VERSION)

all: celluninstall cellinstall sitemodUninstall sitemodInstall

celluninstall:
	@echo "Running celluninstall..."
	make -j$(nproc) celluninstall

cellinstall:
	@echo "Running cellinstall..."
	make -j$(nproc) cellinstall

sitemodUninstall:
	rm -rf $(CellmodSeedInstallDir)

.PHONY: sitemod
sitemodInstall:
	mkdir -p $(CellmodSeedInstallDir)
	rsync -av --ignore-existing --delete $(CellmodSeedDir)/ $(CellmodSeedInstallDir)
