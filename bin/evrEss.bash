#!/bin/bash

# Define: PEVR=[IOCPREFIX] PCIID=[AdressOfEvr] variables before the usage.

MRFIOC2_LATEST=$(ls -1t ${EPICS_DRIVER_PATH}/mrfioc2/ | head -1)
${EPICS_DRIVER_PATH}/mrfioc2/${MRFIOC2_LATEST}/evrEss.cmd # For a particular EVR.
