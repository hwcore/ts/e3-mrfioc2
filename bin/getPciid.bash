#!/bin/bash

# Use only for systems with one EVR
PCIID=$(lspci | grep Xilinx | awk '{print $1}')

echo ${PCIID}
